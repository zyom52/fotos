## Wie man Schach spielt
Wenn Sie das Spiel starten, werden Sie zunächst gefragt, ob Sie gegen einen Computer (Künstliche Intelligenz, AI) oder gegen einen anderen menschlichen Spieler spielen möchten. Sie können dies für jeden Spieler (Weiß und Schwarz) individuell festlegen.

1- Auswahl des Spielmodus: Nach dem Start des Spiels werden Sie mit den folgenden Fragen konfrontiert:

![App Screenshot](https://git.thm.de/zyom52/fotos/-/blob/master/ChessGame/Screenshot%20from%202023-07-12%2009-03-19.png)


Wenn Sie gegen einen anderen menschlichen Spieler spielen möchten, geben Sie für beide Fragen `N` ein. Wenn Sie gegen den Computer spielen möchten, geben Sie `Y` für den Spieler ein, der von der KI gesteuert werden soll (entweder Weiß oder Schwarz).

2- Anpassung der Spielstärke: 

Wenn Sie gegen den Computer spielen, werden Sie aufgefordert, die Schwierigkeitsstufe des Spiels auszuwählen. Sie können dies tun, indem Sie

 `E` für Easy (leicht),

 `M` für Medium (mittel) oder

 `H` für Hard (schwer) eingeben
 
 . Die gewählte Schwierigkeitsstufe beeinflusst die Fähigkeiten und Strategien der KI.

ADD SCREENSHOT HIER

3- Züge machen:

 Um einen Zug zu machen, geben Sie die Start- und Zielkoordinaten Ihres Zuges ein. Die Koordinaten sollten im Format "A1" bis "H8" eingegeben werden, wobei A bis H die Spalten und 1 bis 8 die Reihen auf dem Schachbrett repräsentieren.

ADD SCREENSHOT HIER


4- Spielende:

 Das Spiel endet, wenn ein König schachmatt gesetzt wird 